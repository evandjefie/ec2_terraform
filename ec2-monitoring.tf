# Vars
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_vpc_id" {}

variable "key_name" {}
variable "private_key_path" {}
variable "ssh_user" {}
# variable "ssh_host" {}
# variable "inventory" {}
variable "playbook" {}


# Init
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }
}

provider "aws" {
  profile    = "default"
  region     = "us-west-2"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

# Create default vpc
resource "aws_default_vpc" "default_vpc" {
  tags = {
    Name = "default_vpc"
  }
}

# Get availability zones
#data "aws_availability_zones" "available_zones" {}

# Create default subnet
#resource "aws_default_subnet" "default_subnet" {
#  availability_zone = data.aws_availability_zones.available_zones.names[0]

#  tags = {
#    Name = "default_subnet"
#  }
#}

# Create security group
resource "aws_security_group" "secu_monitoring" {
  name   = "monitoring_srv"
#  vpc_id = aws_default_vpc.default_vpc.id
  vpc_id = var.aws_vpc_id


  ingress {
    description = "ssh access"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "prometheus access"
    from_port   = 9090
    to_port     = 9090
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "grafana access"
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Select ami image
# data "aws_ami" "ubuntu" {
#   most_recent = true

#   filter {
#     name   = "name"
#     values = ["ubuntu/images/hvm-ssd/ubuntu-focal-22.04-amd64-server-*"]
#   }

#   filter {
#     name   = "virtualization-type"
#     values = ["hvm"]
#   }

#   owners = ["099720109477"] # Canonical
# }

resource "aws_instance" "inst_monitoring" {
  ami = "ami-017fecd1353bcc96e" # ubuntu server 22.04
  # ami-a0cfeed8
  # subnet_id     =  
  instance_type               = "t2.micro"
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.secu_monitoring.id]
  key_name                    = var.key_name

  tags = {
    Name = "monitoring_srv"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = var.ssh_user
      host        = aws_instance.inst_monitoring.public_ip
      private_key = file(var.private_key_path)
    }

    inline = [
      "echo 'SSH is done!!!' > hello.txt"
    ]
  }

  provisioner "local-exec" {
    command = "sleep 3; ansible-playbook -i ${aws_instance.inst_monitoring.public_ip}, --private-key ${var.private_key_path} ${var.playbook}"
  }
}

output "monito_ip" {
  value = aws_instance.inst_monitoring.public_ip
}


