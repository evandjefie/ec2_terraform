# AWS
#aws_access_key = ""
#aws_secret_key = ""
aws_vpc_id       = "vpc-00cd3617b432da3db"
# key_name and private_key_path
key_name         = "monito_srv_key"
#private_key_path = "~/.ssh/monito_srv_key.pem"
ssh_user         = "ubuntu"
# ssh_host         = ""

# ANSIBLE
# inventory        = ""
playbook = "../monitoring_ansible/playbook.yml"
